# [palto.org](https://palto.org) source codes

<br/>

### Run palto.org on localhost

    # vi /etc/systemd/system/palto.org.service

Insert code from palto.org.service

    # systemctl enable palto.org.service
    # systemctl start palto.org.service
    # systemctl status palto.org.service

http://localhost:4003
